# 代码编写者： 李朔
# 开发时间： 2021/7/14 9:02
import torch
import torch.nn.functional as F
import numpy as np
import matplotlib.pyplot as plt
#1.数据的准备
x_data=torch.Tensor([[1.0],[2.0],[3.0]])
y_data=torch.Tensor([[0],[0],[1]])    #y的取值变为了分类  0为1类，1为1类
#----------------------------------------------#
#2.模型构造
class LogisticRegressionModel(torch.nn.Module):
    #构造函数
    def __init__(self):
        super(LogisticRegressionModel,self).__init__()
        self.linear=torch.nn.Linear(1,1)   #输入输出维数
    #前馈函数
    def forward(self,x):
        #logistic regression
        y_pred=F.sigmoid(self.linear(x))
        return y_pred
#实例化对象
model=LogisticRegressionModel()
print(model)
#3.损失函数：交叉熵 and 优化器
critertion=torch.nn.BCELoss(size_average=False)  #参数为不使用取平方的方式
#优化器
optimizer=torch.optim.SGD(model.parameters(),lr=0.01)

#4.训练过程
for epoch in range(1000):
    y_pred=model(x_data)
    loss=critertion(y_pred,y_data)
    # print("epoch:{0}   loss={1}".format(epoch,loss.item()))
    optimizer.zero_grad()
    loss.backward()
    optimizer.step()
#测试
x=np.linspace(0,10,200)   #在0-10之间进行采样  取得200个样本
x_t=torch.Tensor(x).view((200,1))   #将200个样本化为200行1列的矩阵
y_t=model(x_t)    #将数据集写入模型
y=y_t.data.numpy()

#绘图
plt.plot(x,y)
plt.plot([0,10],[0.5,0.5],c='r')
plt.xlabel('Hours')
plt.ylabel('Probability of Pass')
plt.grid()
plt.show()
