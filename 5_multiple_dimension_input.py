# 代码编写者： 李朔
# 开发时间： 2021/7/20 10:25
import torch
import numpy as np
#数据集
x_data=torch.tensor([[-0.29,0.49,0.18,-0.29,0.00,0.00,-0.53,-0.03],
                     [-0.88,-0.15,0.08,-0.41,0.00,-0.21,-0.77,-0.67],
                     [-0.06,0.84,0.05,0.00,0.00,-0.81,-0.49,-0.63],
                     [-0.88,-0.11,0.08,-0.54,-0.78,-0.61,-0.92,0.00],
                     [0.00,0.38,-0.34,-0.29,-0.60,0.28,0.89,-0.60],
                     [-0.41,0.17,0.21,0.00,0.00,-0.24,-0.89,-0.70],
                     [-0.65,-0.22,-0.18,-0.35,-0.79,-0.08,-0.85,-0.83],
                     [0.18,0.16,0.00,0.00,0.00,0.05,-0.95,-0.73],
                     [-0.76,0.98,0.15,-0.09,0.28,-0.09,-0.93,0.07],
                     [-0.06,0.26,0.57,0.00,0.00,0.00,-0.87,0.10]])
y_data=torch.tensor([[0.0],[1.0],[0.0],[1.0],[0.0],[1.0],[0.0],[1.0],[0.0],[0.0]])


class Model(torch.nn.Module):
    def __init__(self):
        super(Model,self).__init__()
        self.linear1=torch.nn.Linear(8,6)  #从八维输入---》6维输出
        self.linear2=torch.nn.Linear(6,4)  #从6维输入---》4维输出
        self.linear3=torch.nn.Linear(4,1)  #从4维输入----》1维输出
        self.sigmoid=torch.nn.Sigmoid()    #回归函数

    def forward(self,x):
        x=self.sigmoid(self.linear1(x))   #o1
        x=self.sigmoid(self.linear2(x))   #o2
        x=self.sigmoid(self.linear3(x))   #o3
        return x
model=Model()

#损失函数和优化器
critertion=torch.nn.BCELoss(size_average=True)
optimizer=torch.optim.SGD(model.parameters(),lr=0.1)


#训练模型
for epoch in range(1000):
    y_pred=model(x_data)
    loss=critertion(y_pred,y_data)
    print(epoch,loss.item())
    optimizer.zero_grad()
    loss.backward()
    optimizer.step()

