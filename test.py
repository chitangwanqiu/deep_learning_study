# 代码编写者： 李朔
# 开发时间： 2021/7/15 8:56
import torch
import numpy as np
import matplotlib.pyplot as plt

#构建人工数据集
n_data=torch.ones(50,2)  #数据的基本形态
x1=torch.normal(2*n_data,1)
y1=torch.zeros(50)
x2=torch.normal(-2*n_data,1)
y2=torch.ones(50)
x=torch.cat((x1,x2),0).type(torch.FloatTensor)
y=torch.cat((y1,y2),0).type(torch.FloatTensor)
plt.scatter(x.data.numpy()[:,0],x.data.numpy()[:,1],c=y.data.numpy(),s=100,lw=0)
plt.show()