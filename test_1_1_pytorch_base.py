# 代码编写者： 李朔
# 开发时间： 2021/7/15 11:44
import torch
M=torch.rand(1,3)
print("M:",M)
N=torch.rand(2,1)
print("N:",N)
#实现减法操作
# 1.
print(M-N)
# 2.
print(torch.sub(M,N))
# 3,
M.sub_(N)
print(M)