# 代码编写者： 李朔
# 开发时间： 2021/7/15 12:28
import torch
#创建符合正态分布的矩阵P and Q
P=torch.normal(0,0.01,size=(3,2))
Q=torch.normal(0,0.01,size=(4,2))
print(Q)
#对Q矩阵进行转置操作
Q_T=Q.T
print(Q_T)
#矩阵乘法
print(torch.mm(P,Q_T))
