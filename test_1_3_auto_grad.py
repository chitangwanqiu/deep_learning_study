# 代码编写者： 李朔
# 开发时间： 2021/7/15 12:53
import torch
x=torch.tensor(1.0,requires_grad=True)
print(x)
y_1=x**2
with torch.no_grad():
    y_2=x**3
y_3=y_1+y_2
y_3.backward()
print(x.grad)

