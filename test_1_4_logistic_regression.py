# 代码编写者： 李朔
# 开发时间： 2021/7/15 15:28
import torch
import numpy as np
#构建人工数据集
def articial_data_creation(num_r,num_l):
    n_data=torch.ones(num_r,num_l)  #数据的基本形态
    x1=torch.normal(2*n_data,1)
    y1=torch.zeros(num_r)
    x2=torch.normal(-2*n_data,1)
    y2=torch.ones(num_r)
    features=torch.cat((x1,x2),0).type(torch.FloatTensor)
    labels=torch.cat((y1,y2),0).type(torch.FloatTensor)
    labels=labels.view(-1,1)
    return features,labels
features,labels=articial_data_creation(100,2)
#定义logistic回归
def logistic_regression(w,b,x):
    return 1/(1+torch.exp(-(torch.mm(x,w))+b))
#定义损失函数
loss_cerition=torch.nn.BCEWithLogitsLoss()
#定义优化器
def sgd(params,lr):
    for param in params:
        param.data-=lr*param.grad/100

#初始化训练参数
# w=torch.tensor(1.4,dtype=torch.float32,requires_grad=True)
w=torch.tensor(np.random.normal(0,0.01,(2,1)),dtype=torch.float32,requires_grad=True)
b=torch.tensor(0,dtype=torch.float32,requires_grad=True)
lr=0.01
epochs=1000
#开始训练
for epoch in range(epochs):
    logistic_pred_labels=logistic_regression(w,b,features)
    loss=loss_cerition(logistic_pred_labels,labels)
    # loss=Loss_fun(logistic_pred_labels,logistic_labels)
    mask=logistic_pred_labels.ge(0.5).float()
    correct=(mask==labels).sum()
    acc=correct.data.item()/features.size(0)
    print("epoch:{0}---loss:{1}----acc:{2}".format(epoch,loss,acc))
    loss.backward()
    sgd([w,b],lr)