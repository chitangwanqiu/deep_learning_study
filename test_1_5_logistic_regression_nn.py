# 代码编写者： 李朔
# 开发时间： 2021/7/15 16:52
import torch
import torch.nn.functional as F
#构建人工数据集
def articial_data_creation(num_r,num_l):
    n_data=torch.ones(num_r,num_l)  #数据的基本形态
    x1=torch.normal(2*n_data,1)
    y1=torch.zeros(num_r)
    x2=torch.normal(-2*n_data,1)
    y2=torch.ones(num_r)
    features=torch.cat((x1,x2),0).type(torch.FloatTensor)
    labels=torch.cat((y1,y2),0).type(torch.FloatTensor)
    labels=labels.view(-1,1)
    return features,labels
features,labels=articial_data_creation(100,2)
#2.模型构造
class LogisticRegressionModel(torch.nn.Module):
    #构造函数
    def __init__(self):
        super(LogisticRegressionModel,self).__init__()
        self.linear=torch.nn.Linear(2,1)   #输入:2维 输出维数：1维
    #前馈函数
    def forward(self,x):
        #logistic regression
        y_pred=F.sigmoid(self.linear(x))
        return y_pred
#实例化对象
model=LogisticRegressionModel()
#3.损失函数：交叉熵 and 优化器
critertion=torch.nn.BCELoss(size_average=False)  #参数为不使用取平方的方式
#优化器
optimizer=torch.optim.SGD(model.parameters(),lr=0.01)
#4.训练过程
for epoch in range(1000):
    y_pred=model(features)
    # print(y_pred)
    loss=critertion(y_pred,labels)
    mask=y_pred.ge(0.5).float()
    correct=(mask==labels).sum()
    acc=correct.data.item()/features.size(0)
    print("epoch:{0}   loss={1}    acc={2}".format(epoch,loss.item(),acc))
    optimizer.zero_grad()
    loss.backward()
    optimizer.step()

