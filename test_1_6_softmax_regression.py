# 代码编写者： 李朔
# 开发时间： 2021/7/16 14:51
import torch
import sys
import torchvision
import torchvision.transforms as transforms   #常用的图片变换
import matplotlib.pyplot as plt
import numpy as np

#加载数据集
#训练集
mnist_train=torchvision.datasets.FashionMNIST(root='~/Datasets/FashionMNIST',train=True,download=True,transform=transforms.ToTensor())
#测试集
mnist_test=torchvision.datasets.FashionMNIST(root='~/Datasets/FashionMNIST',train=False,download=True,transform=transforms.ToTensor())
#将对应的标量类别转换成文本标签
def get_fashion_mnist_labels(labels):
    text_labels=['t-shirt','trouser','pullover','dress','coat','sandal','shirt','sneaker','bag','ankleboot']
    return [text_labels[int(i)] for i in labels]
#小批量读取数据
batch_size=256
if sys.platform.startswith('win'):
    num_workers=0
else:
    num_workers=4
#我们在该项目中用到的小批量的数据:训练集-测试集
train_iter=torch.utils.data.DataLoader(mnist_train,batch_size=batch_size,shuffle=True,num_workers=num_workers)
test_iter=torch.utils.data.DataLoader(mnist_test,batch_size=batch_size,shuffle=False,num_workers=num_workers)

#######每个样本输入高和宽都是28个像素，像素的输入向量的长度是28*28=784
#输出个数是10:十类概率
num_inputs=784
num_outputs=10
#初始化模型变量
#w为784*10矩阵；b为1*10矩阵:初始化参数
w=torch.tensor(np.random.normal(0,0.01,(num_inputs,num_outputs)),dtype=torch.float,requires_grad=True)
b=torch.zeros(num_outputs,dtype=torch.float,requires_grad=True)
#定义softmax运算
def softmax(x):
    x_exp=x.exp()
    partition=x_exp.sum(dim=1,keepdim=True)
    return x_exp/partition   #应用广播机制
#定义模型
def net(x):
    return softmax(torch.mm(x.view((-1,num_inputs)),w)+b)
#定义交叉熵损失函数
def cross_entropy(y_hat,y):
    return -torch.log(y_hat.gather(1,y.view(-1,1)))

def accuracy(y_hat,y):
    return (y_hat.argmax(dim=1)==y).float().mean().item()
#正确率评估函数
def evaluate_accuracy(data_iter,net):
    acc_sum,n=0.0,0
    for x,y in data_iter:
        acc_sum+=(net(x).argmax(dim=1)==y).float().sum().item()
        n+=y.shape[0]
    return acc_sum/n
#优化函数:梯度下降
def sgd(params,lr,batch_size):
    for param in params:
        param.data-=lr*param.grad/batch_size
############################
# 训练模型
num_epochs,lr=5,0.1
def train_softmax(net,train_iter,test_iter,loss,num_epochs,batch_size,params=None,lr=None,optimizer=None):
    acc_list=[]
    for epoch in range(num_epochs):
        train_l_sum,train_acc_sum,n=0.0,0.0,0
        for x,y in train_iter:
            y_hat=net(x)
            l=loss(y_hat,y).sum()
            #梯度去除
            if optimizer is not None:
                optimizer.zero_grad()
            elif params is not None and params[0].grad is not None:
                for param in params:
                    param.grad.data.zero_()
            #反馈梯度求算
            l.backward()
            #优化器
            if optimizer is None:
                sgd(params,lr,batch_size)
            else:
                optimizer.step()
            train_l_sum+=l.item()
            train_acc_sum+=(y_hat.argmax(dim=1)==y).sum().item()
            n+=y.shape[0]
        #正确率-测试集
        test_acc=evaluate_accuracy(test_iter,net)
        acc_list.append(test_acc)
        print("epoch:{0}\tloss:{1}\ttrain_acc:{2}\ttest_acc:{3}".format(epoch+1,train_l_sum/n,train_acc_sum/n,test_acc))
    plt.plot(acc_list)
    plt.xlabel('epoch')
    plt.ylabel('test_acc')
    plt.show()
train_softmax(net,train_iter,test_iter,cross_entropy,num_epochs,batch_size,[w,b],lr)

# x,y=iter(test_iter).next()
# true_labels=get_fashion_mnist_labels(y.numpy())
# pred_labels=get_fashion_mnist_labels(net(x).argmax(dim=1).numpy())
# print(true_labels[:10])
# print(pred_labels[:10])

