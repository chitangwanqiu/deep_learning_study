# 代码编写者： 李朔
# 开发时间： 2021/7/17 22:15
import torch
import sys
import matplotlib.pyplot as plt
import torchvision.transforms as transforms   #常用的图片变换
import torchvision
#1.加载数据集
#训练集-测试集
mnist_train=torchvision.datasets.FashionMNIST(root='~/Datasets/FashionMNIST',train=True,download=True,transform=transforms.ToTensor())
mnist_test=torchvision.datasets.FashionMNIST(root='~/Datasets/FashionMNIST',train=False,download=True,transform=transforms.ToTensor())
#将对应的标量类别转换成文本标签
def get_fashion_mnist_labels(labels):
    text_labels=['t-shirt','trouser','pullover','dress','coat','sandal','shirt','sneaker','bag','ankleboot']
    return [text_labels[int(i)] for i in labels]
#小批量读取数据
batch_size=256
if sys.platform.startswith('win'):
    num_workers=0
else:
    num_workers=4
train_iter=torch.utils.data.DataLoader(mnist_train,batch_size=batch_size,shuffle=True,num_workers=num_workers)
test_iter=torch.utils.data.DataLoader(mnist_test,batch_size=batch_size,shuffle=False,num_workers=num_workers)

# 2.定义和初始化模型
num_inputs=784
num_outputs=10
class LinearNet(torch.nn.Module):
    def __init__(self,num_inputs,num_outputs):
        super(LinearNet,self).__init__()
        #输入维度为784,输出维度为10
        self.linear=torch.nn.Linear(num_inputs,num_outputs)
    def forward(self,x):
        y=self.linear(x.view(x.shape[0],-1))
        return y
net=LinearNet(num_inputs,num_outputs)
#3.初始化权重参数
torch.nn.init.normal_(net.linear.weight,mean=0,std=0.01)
torch.nn.init.constant_(net.linear.bias,val=0)

#softmax运算和损失函数混合运算
loss=torch.nn.CrossEntropyLoss()
#优化器
optimizer=torch.optim.SGD(net.parameters(),lr=0.1)

def evaluate_accuracy(data_iter,net):
    acc_sum,n=0.0,0
    for x,y in data_iter:
        acc_sum+=(net(x).argmax(dim=1)==y).float().sum().item()
        n+=y.shape[0]
    return acc_sum/n
#模型训练函数
num_epochs=5
def train_softmax(net,train_iter,test_iter,loss,num_epochs,batch_size,params=None,lr=None,optimizer=None):
    acc_list=[]
    for epoch in range(num_epochs):
        train_l_sum,train_acc_sum,n=0.0,0.0,0
        for x,y in train_iter:
            y_hat=net(x)
            l=loss(y_hat,y).sum()
            #梯度清楚
            if optimizer is not None:
                optimizer.zero_grad()
            elif params is not None and params[0].grad is not None:
                for param in params:
                    param.grad.data.zero_()
            #反馈求梯度
            l.backward()
            #参数更新
            optimizer.step()
            train_l_sum+=l.item()
            train_acc_sum+=(y_hat.argmax(dim=1)==y).sum().item()
            n+=y.shape[0]
        #测试集正确率
        test_acc=evaluate_accuracy(test_iter,net)
        acc_list.append(test_acc)
        print("epoch:{0}\tloss:{1}\ttrain_acc:{2}\ttest_acc:{3}".format(epoch+1,train_l_sum/n,train_acc_sum/n,test_acc))
    #绘图
    plt.plot(acc_list)
    plt.xlabel('epoch')
    plt.ylabel('acc')
    plt.show()
train_softmax(net,train_iter,test_iter,loss,num_epochs,batch_size,None,None,optimizer)