# 代码编写者： 李朔
# 开发时间： 2021/7/17 9:47
import torch
# x=torch.tensor([[1,2,3],[4,5,6]])
# print(x.sum(dim=0,keepdim=True)) #同一列元素求和
# print(x.sum(dim=1,keepdim=True))  #同一行元素求和

def softmax(x):
    x_exp=x.exp()
    # print(x_exp)
    partition=x_exp.sum(dim=1,keepdim=True)
    # print(partition)
    return x_exp/partition   #应用广播机制
# print(softmax(x))

# def net(x):
    # return softmax(torch.mm(x.view((-1,num_inputs)),w)+b)

y_hat=torch.tensor([[0.1,0.3,0.6],[0.3,0.2,0.5]])
y=torch.LongTensor([0,2])
print(y)
print(y.view(-1,1))
y_hat.gather(1,y.view(-1,1))
#交叉熵损失函数
def cross_entropy(y_hat,y):
    return -torch.log(y_hat.gather(1,y.view(-1,1)))

def accuracy(y_hat,y):
    return (y_hat.argmax(dim=1)==y).float().mean().item()

print(accuracy(y_hat[0:10],y[0:10]))